import os

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import keras_tuner as kt
import tensorflow as tf
from keras.datasets import mnist
from keras.layers import Dense
from plot_data import mlp_plots
from metrics import recall_m, precision_m, f1_m
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=200)

(x_train, y_train), (x_test, y_test) = mnist.load_data()

# building the input vector from the 28x28 pixels
# x_train = x_train.reshape(60000, 784)
# x_test = x_test.reshape(10000, 784)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

# normalizing the data to help with the training
x_train /= 255
x_test /= 255

n_classes = 10

y_train = tf.keras.utils.to_categorical(y_train, n_classes)
y_test = tf.keras.utils.to_categorical(y_test, n_classes)


def build_model(hp):
    model = tf.keras.Sequential()

    hp_units1 = hp.Choice('units1', values=[64, 128])
    hp_units2 = hp.Choice('units2', values=[256, 512])
    hp_l2 = hp.Choice('l2', values=[0.1, 0.001, 0.000001])

    initializer = tf.keras.initializers.HeNormal()
    regularizer = tf.keras.regularizers.L2(l2=hp_l2)

    model.add(tf.keras.layers.Flatten(input_shape=(28, 28)))
    model.add(Dense(units=hp_units1, activation=tf.nn.relu, input_shape=(784,), kernel_initializer=initializer,
                    kernel_regularizer=regularizer))
    model.add(
        Dense(units=hp_units2, activation=tf.nn.relu, kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(Dense(10, activation=tf.nn.softmax))

    hp_learning_rate = hp.Choice('learning_rate', values=[0.1, 0.01, 0.001])

    model.compile(
        optimizer=tf.keras.optimizers.RMSprop(learning_rate=hp_learning_rate),
        loss=tf.keras.losses.CategoricalCrossentropy(),
        metrics=[f1_m, precision_m, recall_m, 'accuracy']
    )

    return model


# HyperBand algorithm from keras tuner
tuners = kt.RandomSearch(
    build_model,
    objective=kt.Objective("f1_ m", direction="max"),
    directory='keras_tuner_dir',
    project_name='keras_tuner_demo',
    overwrite=True
)


def tuner(tuners):
    tuners.search(x_test, y_test, epochs=1000, validation_split=0.2, callbacks=[callback])


def evaluate_tuner(tuner):
    best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]
    model = tuner.hypermodel.build(best_hps)
    history = model.fit(x_train, y_train, epochs=100, validation_split=0.2,callbacks=[callback])
    mlp_plots(history)

    # Evaluate the test performance of the tuned model
    loss, accuracy, f1_score, precision, recall = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', loss)
    print('Test accuracy:', accuracy)
    print('Test f1_score:', f1_score)
    print('Test precision:', precision)
    print('Test recall:', recall)


tuner(tuners)
evaluate_tuner(tuners)