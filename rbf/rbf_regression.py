import numpy as np
import tensorflow as tf
from keras import datasets
from plot_data import rbf_plots
from metrics import coeff_determination
from preprocessing import data_normalization, k_means_cluster, center_distance
from rbf_keras import rbflayer, kmeans_initializer

# Importing Dataset
(x_train, y_train), (x_test, y_test) = datasets.boston_housing.load_data(path="boston_housing.npz", test_split=.25)

# Constants

neuron_size = np.array(
    [int(np.floor(.1 * np.max(x_train.shape))), int(np.floor(.5 * np.max(x_train.shape))), int(np.floor(.9 * np.max(x_train.shape)))])
dense_layer_size = 128
output_layer_size = 1
learning_rate = 10 ** -3

# Preprocessing our Data

x_train = data_normalization(x_train)
x_test = data_normalization(x_test)

# Implementing RBF Classifier

def rbf_classifier(neuron_size):

    # Implementing KMeans clustering and calculating max distance between centers.

    centers, x_tran = k_means_cluster(x_train, neuron_size)
    cluster_center_max_dif = max(center_distance(centers))

    sigma = cluster_center_max_dif / np.sqrt(2 * neuron_size)
    betas = (2 * sigma ** 2) ** -1
    model = tf.keras.models.Sequential()

    rbfLayer = rbflayer.RBFLayer(neuron_size,
                                 initializer=tf.constant_initializer(centers),
                                 betas=betas,
                                 input_shape=(13,)
                                 )
    model.add(rbfLayer)
    model.add(tf.keras.layers.Dense(dense_layer_size, ))
    model.add(tf.keras.layers.Dense(output_layer_size, ))

    model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=learning_rate),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.RootMeanSquaredError(), coeff_determination, 'accuracy']
                  )

    history = model.fit(x_train,
                        y_train,
                        batch_size=16,
                        epochs=100,
                        validation_split=.2
                        )

    rbf_plots(history)

rbf_classifier(neuron_size[1])

