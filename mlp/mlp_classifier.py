import os

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import tensorflow as tf
from keras.datasets import mnist
from datetime import datetime
from plot_data import mlp_plots

(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_validation = x_train[-int(x_train.shape[0] * .2):]
x_train = x_train[:-int(x_train.shape[0] * .2)]

y_validation = y_train[-int(y_train.shape[0] * .2):]
y_train = y_train[:-int(y_train.shape[0] * .2)]

# x_train = tf.keras.utils.normalize(x_train, axis=1, order=2)
# x_test = tf.keras.utils.normalize(x_test, axis=1, order=2)
# x_validation = tf.keras.utils.normalize(x_validation, axis=1, order=2)


def different_batch_sizes(batch_size_counter):
    batch_size_list = [1, 256, x_train.shape[0]]

    start_time = datetime.now()

    model = tf.keras.models.Sequential()

    model.add(tf.keras.layers.Flatten(input_shape=(28, 28)))
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(256, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))

    model.compile(optimizer=tf.keras.optimizers.RMSprop(),
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(),
                  metrics=['accuracy']
                  )

    history = model.fit(x_train,
                        y_train,
                        epochs=100,
                        batch_size=batch_size_list[batch_size_counter],
                        validation_data=(x_validation, y_validation),
                        )

    with open('runtime', 'a') as f:
        f.write('\n\rRMSprop with batch size: ' + str(batch_size_list[batch_size_counter]) + '\n' + 'Start Time:' + str(
            start_time) + '   End Time:' + str(datetime.now()) + '   Runtime:' + str(datetime.now() - start_time))
        f.close()

    mlp_plots(history)


def RMSprop_optimizer(rho_counter):
    rho = [0.01, 0.99]
    start_time = datetime.now()

    model = tf.keras.models.Sequential()

    model.add(tf.keras.layers.Flatten(input_shape=(28, 28)))
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(256, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))

    model.compile(optimizer=tf.keras.optimizers.RMSprop(learning_rate=0.001, rho=rho[rho_counter]),
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(),
                  metrics=['accuracy']
                  )

    history = model.fit(x_train,
                        y_train,
                        epochs=100,
                        batch_size=256,
                        validation_data=(x_validation, y_validation),
                        )

    running_time = datetime.now() - start_time

    mlp_plots(history)

    with open('runtime', 'a') as f:
        f.write('\n\rRMSprop with rho:' + str(rho[rho_counter]) + '\n' + 'Start Time:' + str(
            start_time) + '   End Time:' + str(
            datetime.now()) + '   Runtime:' + str(datetime.now() - start_time))
        f.close()



def SGD_optimizer():
    start_time = datetime.now()

    model = tf.keras.models.Sequential()

    initializer = tf.keras.initializers.RandomNormal(mean=10., stddev=1., seed=4)
    regularizer = tf.keras.regularizers.L2(l2=0.01)

    model.add(tf.keras.layers.Flatten(input_shape=(28, 28)))
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu, kernel_initializer=initializer,
                                    kernel_regularizer=regularizer))
    model.add(tf.keras.layers.Dense(256, activation=tf.nn.relu, kernel_initializer=initializer,
                                    kernel_regularizer=regularizer))
    model.add(tf.keras.layers.Dense(10,kernel_initializer=initializer, activation=tf.nn.softmax))

    model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=0.01),
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(),
                  metrics=['accuracy']
                  )

    history = model.fit(x_train,
                        y_train,
                        epochs=100,
                        batch_size=256,
                        validation_data=(x_validation, y_validation),
                        )

    mlp_plots(history)

    with open('runtime', 'a') as f:
        f.write('\n\rSGD optimizer with L2 reguralization' + '\n' + 'Start Time:' + str(
            start_time) + '   End Time:' + str(
            datetime.now()) + '   Runtime:' + str(datetime.now() - start_time))
        f.close()



def SGD_optimizer_L2(alpha_counter):
    alpha = [0.1, 0.01, 0.001]
    start_time = datetime.now()

    model = tf.keras.models.Sequential()


    initializer = tf.keras.initializers.RandomNormal(mean=.7, stddev=1., seed=4)
    regularizer = tf.keras.regularizers.L2(l2=alpha[alpha_counter])

    model.add(tf.keras.layers.Flatten(input_shape=(28, 28)))
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu, kernel_initializer=initializer,
                                    kernel_regularizer=regularizer))
    model.add(tf.keras.layers.Dense(256, activation=tf.nn.relu, kernel_initializer=initializer,
                                    kernel_regularizer=regularizer))
    model.add(tf.keras.layers.Dense(10,  kernel_initializer=initializer,
                                    kernel_regularizer=regularizer, activation=tf.nn.softmax))

    model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=0.01),
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy']
                  )

    history = model.fit(x_train,
                        y_train,
                        epochs=100,
                        batch_size=256,
                        validation_data=(x_validation, y_validation),
                        )

    mlp_plots(history)

    with open('runtime', 'a') as f:
        f.write('\n\rSGD optimizer with L2 reguralization and alpha:' + str(
            alpha[alpha_counter]) + '\n' + 'Start Time:' + str(
            start_time) + '   End Time:' + str(
            datetime.now()) + '   Runtime:' + str(datetime.now() - start_time))
        f.close()


def SGD_l1_optimizer():
    start_time = datetime.now()

    model = tf.keras.models.Sequential()

    initializer = tf.keras.initializers.RandomNormal(mean=10, stddev=1., seed=4)
    regularizer = tf.keras.regularizers.L1(l1=0.01)

    model.add(tf.keras.layers.Flatten(input_shape=(28, 28)))
    model.add(tf.keras.layers.Dense(128,kernel_regularizer=regularizer, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(256,kernel_regularizer=regularizer, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(10,kernel_regularizer=regularizer, activation=tf.nn.softmax))
    model.add(tf.keras.layers.Dropout(rate=0.3))


    model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=0.01),
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy']
                  )

    history = model.fit(x_train,
                        y_train,
                        epochs=100,
                        batch_size=256,
                        validation_data=(x_validation, y_validation),
                        )

    running_time = datetime.now() - start_time

    mlp_plots(history)

    with open('runtime', 'a') as f:
        f.write('\n\rSGD optimizer with L1 normalization' + '\n' + 'Start Time:' + str(
            start_time) + '   End Time:' + str(
            datetime.now()) + '   Runtime:' + str(datetime.now() - start_time))
        f.close()


SGD_l1_optimizer()


