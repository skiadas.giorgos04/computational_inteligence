function [data] = normalize_data(input)
    data_temp  = input(:,1:end-1);
    
    [~,columns] =  size(data_temp);
    cols_mean = mean(data_temp,1);
    cols_std = std(data_temp,1,1);
    for i = 1:columns-1
        data_temp(:,i) = (data_temp(:,i)-cols_mean(i))./ cols_std(i);
    end
    data = [data_temp input(:,end)];
    
end