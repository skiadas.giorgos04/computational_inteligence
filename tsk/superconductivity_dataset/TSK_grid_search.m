clc
clear

%% Import dataset

data = csvread("train.csv",1,1);

%% Initializing grid search
characteristics_size = 5;
radius_size = 5;

grid_search = zeros(characteristics_size,radius_size,2);

chars_value = 4;
chars_step = 2;
for i = 1:characteristics_size
    grid_search(i,:,1) = chars_value;
    chars_value = chars_value + chars_step;
end

radius_value = 0.2;
radius_step = 0.1;

for i = 1:radius_size
    grid_search(:,i,2) = radius_value;
    radius_value = radius_value + radius_step;
end

k = 5;
errors = zeros(characteristics_size,radius_size,5);


for w = 1:characteristics_size
    for z = 1:radius_size
        number_of_features = grid_search(w,z,1);
        radius = grid_search(w,z,2);

        cross_val_OA = zeros(k,1);
        partition = cvpartition(data(:,end),'KFold',5,'Stratify',true);

        for iteration = 1:k
            % train test validation split dataset
            train_set_temp = data(training(partition,iteration),:);
            test_set = data(test(partition,iteration),:);

            val_partition = cvpartition(train_set_temp(:,end),'KFold',4,'Stratify',true);
            train_set = train_set_temp(training(val_partition,1),:);
            validation_set = train_set_temp(test(val_partition,1),:);
           
            % feature selection
            [index,weights] = relieff(train_set(:,1:end-1),train_set(:,end),5);
    
            train_set_FS = train_set(:,index(1:number_of_features));
            train_set_FS = [train_set_FS train_set(:,end)];
    
            test_set_FS = test_set(:,index(1:number_of_features));
            test_set_FS = [test_set_FS test_set(:,end)];
 
            validation_set_FS = validation_set(:,index(1:number_of_features));
            validation_set_FS = [validation_set_FS validation_set(:,end)];


            % clustering
            [c1,sig1] = subclust(train_set_FS(train_set_FS(:,end) == 1,:),radius);
            [c2,sig2] = subclust(train_set_FS(train_set_FS(:,end) == 2,:),radius);
            [c3,sig3] = subclust(train_set_FS(train_set_FS(:,end) == 3,:),radius);
            [c4,sig4] = subclust(train_set_FS(train_set_FS(:,end) == 4,:),radius);
            [c5,sig5] = subclust(train_set_FS(train_set_FS(:,end) == 5,:),radius);
            num_rules = size(c1,1) + size(c2,1) + size(c3,1) + size(c4,1) + size(c5,1);

            % Build FIS From Scratch
            fis = newfis('FIS_SC','sugeno');

            % Add Input-Output Variables
            names_in = {};
            for i = 1:size(train_set_FS,2)-1
                num = int2str(i);
                name = 'input';
                name = strcat(name,num);
                names_in = [names_in name];
            end
            for i = 1:size(train_set_FS,2)-1
                fis = addvar(fis,'input',names_in{i},[0 1]);
            end
            fis = addvar(fis,'output','out1',[0 1]);
            
            
            mf_names = strings(10000,1);
            for i = 1:10000
                mf_names(i) = "mf"+i;
            end
            % Add Input Membership Functions
            counter = 1;
            for i = 1:size(train_set_FS,2)-1
                for j = 1:size(c1,1)
                    fis = addmf(fis,'input',i,mf_names(counter,1),'gbellmf',[0.5 2.5 c1(j,i)]);
                    counter = counter + 1;

                end
                for j = 1:size(c2,1)
                    fis = addmf(fis,'input',i,mf_names(counter,1),'gbellmf',[0.5 2.5 c2(j,1)]);
                   counter = counter + 1;
                end
                for j = 1:size(c3,1)
                    fis = addmf(fis,'input',i,mf_names(counter,1),'gbellmf',[0.5 2.5 c3(j,1)]);
                    counter = counter + 1;
                end
                for j = 1:size(c4,1)
                    fis = addmf(fis,'input',i,mf_names(counter,1),'gbellmf',[0.5 2.5 c4(j,1)]);
                    counter = counter + 1;
                end
                for j = 1:size(c5,1)
                    fis = addmf(fis,'input',i,mf_names(counter,1),'gbellmf',[0.5 2.5 c5(j,1)]);
                    counter = counter + 1;
                end
            end

            temp2 = zeros(1,size(c2,1));
            temp3 = zeros(1,size(c3,1));
            temp4 = zeros(1,size(c4,1));
            temp2 = temp2 + 0.25;
            temp3 = temp3 + 0.5;
            temp4 = temp4 + 0.75;
            
            % Add Output Membership Functions
            params = [zeros(1,size(c1,1)) temp2 temp3 temp4 ones(1,size(c5,1))];
            counter = 1;
            for i = 1:num_rules
                fis = addmf(fis,'output', 1,mf_names(counter,1) ,'constant',params(i));
                counter = counter+1;
            end

            % Add FIS Rule Base
            rule_list = zeros(num_rules,size(train_set_FS,2));
            for i = 1:size(rule_list,1)
                rule_list(i,:) = i;
            end
    
            rule_list = [rule_list,ones(num_rules,2)];
            fis = addrule(fis,rule_list);
    
            [train_fis, train_error,~,validation_fis ,validation_error] = anfis(train_set_FS,fis,[150 0 0.01 0.9 1.1],[],validation_set_FS);
            
            %Learning Curve
            figure()
            plot([train_error, validation_error],'LineWidth',2);grid on;
            xlabel = "Number of Epochs";
            ylabel = "Errors";

            y_predict = evalfis(test_set_FS(:,1:end-1),validation_fis);
            y_predict = round(y_predict);
            diff = test_set_FS(:,end) - y_predict;
            
           [MSE,RMSE,R_squared,NMSE,NDEI] = metrics(test_set_FS(:,end),y_predict);
        end
    end 
    errors(w,z,:) = [MSE,RMSE,R_squared,NMSE,NDEI];
    print("end first iteration");
end



function [MSE,RMSE,R_squared,NMSE,NDEI] = metrics(expected_output, calculated_output)
    squared_dif = (expected_output - calculated_output).^2;
    MSE = sum(squared_dif)*size(squared_dif,1)^-1;
    RMSE = sqrt(MSE);
    
    ss_res = sum(squared_dif);
    output_mean = mean(expected_output);
    ss_tot = sum((expected_output-output_mean).^2);
    R_squared = 1 - (ss_res / ss_tot);
    
    NMSE = ss_res / ss_tot;
    NDEI = sqrt(NMSE);
end
