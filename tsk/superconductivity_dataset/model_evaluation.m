clc 
clear

data = csvread("train.csv",1,1);
data = normalize_data(data);

number_of_features = 8;
radius =0.2;

% % K fold cross validation 

k = 5;
cross_val_errors = zeros(k,2);

cross_val_OA = zeros(k,1);
cross_val_PA = zeros(5,k);
cross_val_UA = zeros(5,k);
cross_val_k = zeros(k,1);
cross_val_matrix = zeros(5,5,5);
partition = cvpartition(data(:,end),'KFold',5,'Stratify',true);

for iteration = 1:k
    
    % train test validation split dataset
    train_set_temp = data(training(partition,iteration),:);
    test_set = data(test(partition,iteration),:);
    
    val_partition = cvpartition(train_set_temp(:,end),'KFold',5,'Stratify',true);
    train_set = train_set_temp(training(val_partition,1),:);
    validation_set = train_set_temp(test(val_partition,1),:);
    
    % feature selection
    [index,weights] = relieff(train_set(:,1:end-1),train_set(:,end),5);
    
    train_set_FS = train_set(:,index(1:number_of_features));
    train_set_FS = [train_set_FS train_set(:,end)];
    
    test_set_FS = test_set(:,index(1:number_of_features));
    test_set_FS = [test_set_FS test_set(:,end)];
 
    validation_set_FS = validation_set(:,index(1:number_of_features));
    validation_set_FS = [validation_set_FS validation_set(:,end)];

    
    % clustering
    [c1,sig1] = subclust(train_set_FS(train_set_FS(:,end) == 1,:),radius);
    [c2,sig2] = subclust(train_set_FS(train_set_FS(:,end) == 2,:),radius);
    [c3,sig3] = subclust(train_set_FS(train_set_FS(:,end) == 3,:),radius);
    [c4,sig4] = subclust(train_set_FS(train_set_FS(:,end) == 4,:),radius);
    [c5,sig5] = subclust(train_set_FS(train_set_FS(:,end) == 5,:),radius);
    
    number_of_rules = size(c1,1) + size(c2,1) + size(c3,1) +size(c4,1)+ size(c5,1);

    % build fis from scratch
    
    fis = newfis('FIS_SC', 'sugeno');
    
    names_in = {};
    for i = 1:size(train_set_FS,2)-1
        counter = int2str(i);
        name = 'input';
        name = strcat(name, counter);
        names_in = [names_in name];
    end
    for i = 1:size(train_set_FS,2)-1
        fis = addvar(fis,'input',names_in{i},[0,1]);
    end
    fis = addvar(fis,'output','out1',[0,1]);
    
    %Add Input Membership Functions
    
    mf_names = strings(10000,1);
    for i = 1:10000
        mf_names(i) = "mf"+i;
    end
    
    counter = 1;
    for i = 1:size(train_set_FS,2)-1
        for j = 1:size(c1,1)
            fis = addmf(fis,'input',i,mf_names(counter,1),'gbellmf',[0.5 2.5 c1(j,i)]);
            counter = counter + 1;
            
        end
        for j = 1:size(c2,1)
            fis = addmf(fis,'input',i,mf_names(counter,1),'gbellmf',[0.5 2.5 c2(j,1)]);
           counter = counter + 1;
        end
        for j = 1:size(c3,1)
            fis = addmf(fis,'input',i,mf_names(counter,1),'gbellmf',[0.5 2.5 c3(j,1)]);
            counter = counter + 1;
        end
        for j = 1:size(c4,1)
            fis = addmf(fis,'input',i,mf_names(counter,1),'gbellmf',[0.5 2.5 c4(j,1)]);
            counter = counter + 1;
        end
        for j = 1:size(c5,1)
            fis = addmf(fis,'input',i,mf_names(counter,1),'gbellmf',[0.5 2.5 c5(j,1)]);
            counter = counter + 1;
        end
    end
    
    temp2 = zeros(1,size(c2,1));
    temp3 = zeros(1,size(c3,1));
    temp4 = zeros(1,size(c4,1));
    temp2 = temp2 + 0.25;
    temp3 = temp3 + 0.5;
    temp4 = temp4 + 0.75;
    
    params = [zeros(1,size(c1,1)) temp2 temp3 temp4 ones(1,size(c5,1))];
    counter = 1;
    for i = 1:number_of_rules
        fis = addmf(fis,'output', 1,mf_names(counter,1) ,'constant',params(i));
        counter = counter+1;
    end
    
    
    rule_list = zeros(number_of_rules,size(train_set_FS,2));
    for i = 1:size(rule_list,1)
        rule_list(i,:) = i;
    end
    
    rule_list = [rule_list,ones(number_of_rules,2)];
    fis = addrule(fis,rule_list);
    
    [train_fis, train_error,~,validation_fis ,validation_error] = anfis(train_set_FS,fis,[120 0 0.01 0.9 1.1],[],validation_set_FS);
    
    % Plot mf before training
    mf_title = "Membership Function before training, input ";
    figure()
    plotmf(fis,'input',1);
    title1= strcat(mf_title,'1');
    title(title1);
    
    figure()
    plotmf(fis,'input',size(train_set_FS,2)-1);
    counter = int2str(size(train_set_FS,2)-1);
    title2 = strcat(mf_title,counter);
    title(title2);
    
    
    
    
    % Plot mf after training
    mf_title = "Membership Function after training, input ";
    figure()
    plotmf(validation_fis,'input',1);
    title1= strcat(mf_title,'1');
    title(title1);
    
    figure()
    plotmf(validation_fis,'input',size(train_set_FS,2)-1);
    counter = int2str(size(train_set_FS,2)-1);
    title2 = strcat(mf_title,counter);
    title(title2);    
    
end

%Learning Curve
figure()
plot([train_error, validation_error],'LineWidth',2);grid on;
xlabel = "Number of Epochs";
ylabel = "Errors";
title("Learning Rate")

y_predict = evalfis(test_set_FS(:,1:end-1),validation_fis);
y_predict = round(y_predict);
diff = test_set_FS(:,end) - y_predict;

[MSE,RMSE,R_squared,NMSE,NDEI] = metrics(test_set_FS(:,end),y_predict);


figure()
plot(1:size(errors,1),diff,'*k')
ylabel= 'Errors';
xlabel= 'Samples';
title('Error between expected and calculated value, model 4')


function [MSE,RMSE,R_squared,NMSE,NDEI] = metrics(expected_output, calculated_output)
    squared_dif = (expected_output - calculated_output).^2;
    MSE = sum(squared_dif)*size(squared_dif,1)^-1;
    RMSE = sqrt(MSE);
    
    ss_res = sum(squared_dif);
    output_mean = mean(expected_output);
    ss_tot = sum((expected_output-output_mean).^2);
    R_squared = 1 - (ss_res / ss_tot);
    
    NMSE = ss_res / ss_tot;
    NDEI = sqrt(NMSE);
end
