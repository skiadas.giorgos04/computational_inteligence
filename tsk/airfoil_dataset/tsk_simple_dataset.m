clear
clc

%% Import and split dataset

data = load("airfoil_self_noise.dat");
[rows, ~] = size(data);

indeces_train_set = randperm(rows,floor(rows*.6));
train_set = data(indeces_train_set,:);
data(indeces_train_set,:) = [];
[rows, ~] = size(data);

indeces_test_set = randperm(rows,floor(rows*.5));
test_set = data(indeces_test_set,:);
data(indeces_test_set,:) = [];
validation_set = data;


my_fis(1)=genfis1(train_set,2,'gbellmf','constant'); 
my_fis(2)=genfis1(train_set,3,'gbellmf','constant'); 
my_fis(3)=genfis1(train_set,2,'gbellmf','linear'); 
my_fis(4)=genfis1(train_set,3,'gbellmf','linear'); 

% Train fuzzy system
[train_fis, train_error, ~, validation_fis, validation_error] = anfis(train_set,my_fis(4),[100 0 0.01 0.9 1.1],[],validation_set);

[calculated_output,fuzzifiedIn,ruleOut,aggregatedOut,ruleFiring] = evalfis(validation_fis,test_set(:,1:end-1));

figure();
plot([train_error validation_error],'LineWidth',2); grid on;
legend('Training Error','Validation Error');
xlabel('# of Epochs');
ylabel('Error');
title('Training  and validation set cost, model 4')

expected_output = test_set(:,end);

[MSE,RMSE,R_squared,NMSE,NDEI] = metrics(expected_output, calculated_output)

for j = 1:5 
    figure();
    plotmf(validation_fis,'input',j);
    title1 = "Model " + 4 + " Feature " + j;
    title(title1);
end

errors = expected_output - calculated_output;

figure()
plot(1:size(errors,1),errors,'*k')
ylabel('Errors')
xlabel('Samples')
title('Error between expected and calculated value, model 4')

%% Metrics

function [MSE,RMSE,R_squared,NMSE,NDEI] = metrics(expected_output, calculated_output)
    squared_dif = (expected_output - calculated_output).^2;
    MSE = sum(squared_dif)*size(squared_dif,1)^-1;
    RMSE = sqrt(MSE);
    
    ss_res = sum(squared_dif);
    output_mean = mean(expected_output);
    ss_tot = sum((expected_output-output_mean).^2);
    R_squared = 1 - (ss_res / ss_tot);
    
    NMSE = ss_res / ss_tot;
    NDEI = sqrt(NMSE);
end

