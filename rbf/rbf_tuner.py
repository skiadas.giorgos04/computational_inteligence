import os

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import numpy as np
import tensorflow as tf
from keras import datasets
from plot_data import rbf_plots
from metrics import coeff_determination
from preprocessing import data_normalization, k_means_cluster, center_distance
from sklearn.preprocessing import normalize
from rbf_keras import rbflayer
import keras_tuner as kt

# Importing Dataset
(x_train, y_train), (x_test, y_test) = datasets.boston_housing.load_data(path="boston_housing.npz", test_split=.25)

# Constants

dense_layer_size = 128
output_layer_size = 1
learning_rate = 10 ** -3
epochs = 100

# Preprocessing our Data

x_train = normalize(x_train)
x_test = normalize(x_test)


# Implementing RBF Classifier

def build_model(hp):
    # Hyperparameters to be tuned

    rbf_neuron_size = hp.Choice('neuron_size', values=[.05, .15, .3, .5])
    rbf_layer_size = hp.Choice('rbf_layer_size', values=[32, 62, 128, 256])
    dropout_probability = hp.Choice('dropout probability', values=[.2, .35, .5])

    neuron_size = int(np.floor(rbf_neuron_size * np.max(x_train.shape)))

    # Implementing KMeans clustering and calculating max distance between centers.

    centers, x_tran = k_means_cluster(x_train, neuron_size)
    cluster_center_max_dif = max(center_distance(centers))

    sigma = cluster_center_max_dif / np.sqrt(2 * neuron_size)
    betas = (2 * sigma ** 2) ** -1

    model = tf.keras.models.Sequential()

    rbfLayer = rbflayer.RBFLayer(neuron_size,
                                 initializer=tf.constant_initializer(centers),
                                 betas=betas,
                                 input_shape=(13,)
                                 )
    model.add(rbfLayer)
    model.add(tf.keras.layers.Dense(rbf_layer_size, ))
    model.add(tf.keras.layers.Dropout(dropout_probability))
    model.add(tf.keras.layers.Dense(output_layer_size, ))

    model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=learning_rate),
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=[tf.keras.metrics.RootMeanSquaredError(), coeff_determination, ]
                  )

    return model


tuners = kt.RandomSearch(
    build_model,
    objective=kt.Objective("root_mean_squared_error", direction="min"),
    directory='keras_tuner_rbf',
    project_name='rbf_tuner',
    overwrite=True
)


def tuner(tuners):
    tuners.search(x_train, y_train,
                  epochs=epochs, validation_split=0.2, )


def evaluate_tuner(tuner):
    best_hps = tuner.get_best_hyperparameters(num_trials=1)[0]
    model = tuner.hypermodel.build(best_hps)
    history = model.fit(x_train, y_train, epochs=100, validation_split=0.2)
    rbf_plots(history)

    # Evaluate the test performance of the tuned model
    loss, root_mean_squared_error, coeff_determination = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss: ', loss)
    print('Test RMSE: ', root_mean_squared_error)
    print('Test coefficient of determination: ', coeff_determination)

tuner(tuners)
evaluate_tuner(tuners)
