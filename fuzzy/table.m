%% Design System's  Transfer Functions

open_loop_sys = tf(27*[1,.2],[1,10.01,1,0]);
closed_loop_sys = feedback(open_loop_sys,1,-1);
controlSystemDesigner(open_loop_sys);
                
Kp = 1.08;
c = 0.2;
Ki = 6,4;
%% Plots

figure()
rlocus(open_loop_sys)
grid on;    

figure()
step(closed_loop_sys)

grid on;
%% Fuzzy Control 

fuzzyLogicDesigner('table_fuzzy_estimator.fis');
fis = readfis('table_fuzzy_estimator.fis');

input = [0, .25]; % Since ZR = 0 and PS = 0.25

[output,fuzzifiedIn,ruleOut,aggregatedOut,ruleFiring] = evalfis(fis,input);

%% plot 
figure()
outputRange = linspace(fis.output.range(1),fis.output.range(2),length(aggregatedOut))';
plot(outputRange,aggregatedOut,[output output],[-1 1])

ylabel('Output Membership')
legend('Aggregated output fuzzy set','Defuzzified output')

%%
figure()
gensurf(fis)
