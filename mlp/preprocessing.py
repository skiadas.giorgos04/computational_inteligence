from sklearn.preprocessing import StandardScaler
from sklearn import cluster
import numpy as np

def data_normalization(data):
    enc = StandardScaler()
    normalized_data = enc.fit_transform(data)
    return normalized_data

def k_means_cluster(data_x,number_of_clusters):
    clusters = cluster.KMeans(n_clusters=number_of_clusters)
    clusters.fit(data_x)
    return clusters.cluster_centers_, clusters.fit_transform(data_x)

def center_distance(cluster):
    max_dif = np.zeros([cluster.shape[1]])

    for i in range(cluster.shape[0] - 1):
        for j in range(i + 1, cluster.shape[0]):
            dif = abs(cluster[i][:] - cluster[j][:])

        for k in range(cluster.shape[1]):
            if dif[k] > max_dif[k]:
                max_dif[k] = dif[k]

    return max_dif